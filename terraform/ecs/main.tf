resource "aws_iam_role" "ecs_task_role" {
  name = "${var.name}-ecsTaskRole"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "ecs-flow-logs-policy"{
    name = "${var.name}-ecs-flow-logs-policy"
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = "${var.name}-ecsTaskExecutionRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ecs-task-role-policy-attachment1" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = aws_iam_policy.ecs-flow-logs-policy.arn
}

resource "aws_cloudwatch_log_group" "main" {
  name = "/ecs/${var.name}-task"

  tags = {
    Name        = "${var.name}-task"
    Environment = var.environment
  }
}

resource "aws_ecs_task_definition" "main" {
    family                   = "${var.name}-task"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = var.container_cpu
    memory                   = var.container_memory
    task_role_arn            = aws_iam_role.ecs_task_role.arn
    execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
    container_definitions = jsonencode([{
        name        = "${var.name}-container"
        image       = "${var.container_image}:${var.image_tag}"
        essential   = true
        portMappings = [{
        protocol      = "tcp"
        containerPort = var.container_port
        hostPort      = var.container_port
        }]
        logConfiguration = {
        logDriver = "awslogs"
        options = {
            awslogs-group         = aws_cloudwatch_log_group.main.name
            awslogs-stream-prefix = "ecs"
            awslogs-region        = var.region
        }
        }
        
    }])

    tags = {
        Name        = "${var.name}-task"
        Environment = var.environment
    }
}

resource "aws_lb" "main" {
  name               = "${var.name}-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.alb_security_group
  subnets            = var.public_subnets

  enable_deletion_protection = false

  tags = {
    Name        = "${var.name}-alb"
    Environment = var.environment
  }
}

resource "aws_alb_target_group" "main" {
  name        = "${var.name}-tg"
  port        = 5000
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "2"
    port = 5000
  }

  tags = {
    Name        = "${var.name}"
    Environment = var.environment
  }
}

resource "aws_alb_listener" "https" {
    load_balancer_arn = aws_lb.main.id
    port              = 80
    protocol          = "HTTP"
    default_action {
        target_group_arn = aws_alb_target_group.main.id
        type             = "forward"
    }
}

resource "aws_ecs_service" "main" {
  name                               = "${var.name}-service"
  cluster                            = var.ecs_arn
  task_definition                    = aws_ecs_task_definition.main.arn
  desired_count                      = 1
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 200
  health_check_grace_period_seconds  = 60
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"

  network_configuration {
    security_groups  = var.ecs_security_group
    subnets          = var.private_subnets
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.main.id
    container_name   = "${var.name}-container"
    container_port   = var.container_port
  }

  # we ignore task_definition changes as the revision changes on deploy
  # of a new version of the application
  # desired_count is ignored as it can change due to autoscaling policy
  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }
}

output "app_base_url" {
  value = aws_lb.main.dns_name
}