variable "container_cpu" {
  description = "The number of cpu units used by the task"
  default     = 1024
}

variable "container_memory" {
  description = "The amount (in MiB) of memory used by the task"
  default     = 2048
}

variable "name" {
  default = "counter-project"
  type = string
}

variable "environment" {
  default = "dev"
  type = string
}

variable "container_image"{

}

variable "image_tag"{

}

variable "container_port" {
    default = 5000
}

variable "region"{
    default = "ap-southeast-1"
}

variable "alb_security_group" {
  
}

variable "public_subnets" {
  type = list
}

variable "vpc_id" {
  
}

variable "ecs_arn" {
  
}

variable "ecs_security_group" {
  
}

variable "private_subnets" {
  type = list
}