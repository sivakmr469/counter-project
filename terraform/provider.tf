terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = {
        source = "hashicorp/aws"
        version = "3.63.0"
    }
  }

  backend "s3" {
    bucket = "counter-project-app"
    key    = "counter-project-app.tfstate"
    region = "ap-southeast-1"
    dynamodb_table = "counter-project-app"
  }
}

provider "aws"{
    region = "ap-southeast-1"
}