module "ecs"{
    source = "./ecs"
    container_image = var.container_image
    image_tag = var.image_tag
    vpc_id = var.vpc_id
    alb_security_group = var.alb_security_group
    ecs_security_group = var.ecs_security_group
    ecs_arn = var.ecs_arn
    public_subnets = var.public_subnets
    private_subnets = var.private_subnets
}