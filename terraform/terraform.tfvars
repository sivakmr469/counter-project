alb_security_group = [
  "sg-05b9ac71540cdb6e6",
]
container_image = "084767242532.dkr.ecr.ap-southeast-1.amazonaws.com/counter-project"
ecs_arn = "arn:aws:ecs:ap-southeast-1:084767242532:cluster/counter-project-cluster"
ecs_security_group = [
  "sg-059893af3370f2503",
]
private_subnets = [
  "subnet-058a62da6925746a5",
  "subnet-0d265bb5a07e60b30",
]
public_subnets = [
  "subnet-00f7efc2ac2d8ab36",
  "subnet-0018ec35d08c10dca",
]
vpc_id = "vpc-09a2e916f2932fd71"